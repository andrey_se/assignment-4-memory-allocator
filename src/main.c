#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>

void test_memory_allocation() {
    printf("Тест выделения памяти\n");
    void* heap = heap_init(0);
    assert(heap != NULL);
    debug_heap(stdout, HEAP_START);

    void* block = _malloc(25);
    assert(block != NULL);
    debug_heap(stdout, HEAP_START);

    _free(block);
    heap_term();
}

static void test_memory_free() {
    printf("\n\nТест освобождения памяти\n");

    void* heap = heap_init(0);
    assert(heap != NULL);
    debug_heap(stdout, heap);

    void* block1 = _malloc(240);
    assert(block1 != NULL);

    void* block2 = _malloc(480);
    assert(block2 != NULL);

    void* block3 = _malloc(720);
    assert(block3 != NULL);

    debug_heap(stdout, heap);

    _free(block1);
    _free(block2);
    _free(block3);
    heap_term();
}

void test_memory_free_2() {
    printf("\n\nТест освобождения 2\n");
    void* heap = heap_init(0);
    assert(heap != NULL);

    void* block1 = _malloc(240);
    assert(block1 != NULL);
    void* block2 = _malloc(480);
    assert(block2 != NULL);
    void* block3 = _malloc(720);
    assert(block3 != NULL);
    debug_heap(stdout, HEAP_START);

    _free(block3);
    _free(block2);
    debug_heap(stdout, HEAP_START);

    _free(block1);
    heap_term();
}

void test_grow_extends() {
    printf("\n\nТест расширения\n");
    void* heap = heap_init(0);
    assert(heap != NULL);
    debug_heap(stdout, HEAP_START);

    void* block1 = _malloc(8000);
    assert(block1 != NULL);
    debug_heap(stdout, HEAP_START);

    void* block2 = _malloc(8000);
    assert(block2 != NULL);
    debug_heap(stdout, HEAP_START);

    _free(block1);
    _free(block2);
    debug_heap(stdout, HEAP_START);

    heap_term();
}

static void test_grow_no_extends() {
    printf("\nТест без расширения\n");

    void* heap = heap_init(8000);
    assert(heap != NULL);
    debug_heap(stdout, heap);

    void* block = _malloc(8000);
    assert(block != NULL);
    debug_heap(stdout, heap);

    void* reserved = mmap(HEAP_START + REGION_MIN_SIZE, 240, 0, MAP_PRIVATE | 0x20, -1, 0);

    void* block2 = _malloc(1000);
    assert(block2 != NULL);
    debug_heap(stdout, heap);

    munmap(reserved, 1);
    _free(block2);
    _free(block);
    heap_term();
}

int main() {
    test_memory_allocation();
    test_memory_free();
    test_memory_free_2();
    test_grow_extends();
    test_grow_no_extends();
    return 0;
}